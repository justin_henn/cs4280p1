CC	= g++		# The C compiler
CFLAGS	= -g		# Enable debugging by default
TARGET	= scanner
OBJS	= main.o testScanner.o scanner.o

$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS)

main.o:	main.cpp
	$(CC) $(CFLAGS) -c main.cpp

testScanner.o: testScanner.cpp
	$(CC) $(CFLAGS) -c testScanner.cpp

scanner.o: scanner.cpp
	$(CC) $(CFLAGS) -c scanner.cpp

clean:
	/bin/rm -f *.o *~ *.txt *.sp18 $(TARGET)
