//scanner
//This is the scanner implementation
//Justin Henn
//Assignment 1
//3/15/18
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <string.h>
#include "token.h"

using namespace std;


/*arrays for tokens, keyword, and fsa table*/

const int NUM_ROWS = 3, NUM_COLS = 25, ADD_TO_GET_KEYWORD = 2;

const string KEYWORDS[] = {"start", "stop", "iter", "void", "var", "return", "read", "print", "program", "iff", "then", "let"};

const string INSTANCES[] = {"IDTk", "digitTk", "startTk", "stopTk", "iterTk", "voidTk", "varTk", "returnTk", "readTk", "printTk", "programTk", "iffTk", "thenTk", "letTk", "=Tk", "<Tk", ">Tk", ":Tk", "+Tk", "-Tk", "*Tk", "/Tk", "%Tk", ".Tk", "(Tk", ")Tk", ",Tk", "{Tk", "}Tk", ";Tk", "[Tk", "]Tk", "EOFTk"};

const int FSA_TABLE[NUM_ROWS][NUM_COLS] = {{1032, 1031, 1030, 1029, 1028, 1027, 1026, 1025, 1024, 1023, 1022, 1021, 1020, 1019, 1018, 1017, 1016, 1015, 1014, 1, 2, -1, -2, 0, 0 }, {1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1, 1, 1, 1000, 1000, 1000}, {1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 2, 1001, 1001, 1001, 1001}};


/*function to see if incoming charactor is an operator or limiter and return the ascii code*/

int operator_or_limiter(char nextChar, int col) {

  switch (nextChar) {

      case EOF:
	       col = 0;
	       break;
      case ']':
	       col = 1;
	       break;
      case '[':
	       col = 2;
	       break;
      case ';':             
	       col = 3;
	       break;		
      case '}':	
	       col = 4;
	       break;		
      case '{':	
	       col = 5;
	       break;	
      case ',':
	       col = 6;
	       break;	
      case ')':	
	       col = 7;
	       break;		
      case '(':	
	       col = 8;
	       break;		
      case '.':
	       col = 9;
	       break;		
      case '%':	
	       col = 10;
	       break;
      case '/':	
	       col = 11;
	       break;	
      case '*':	
	       col = 12;
	       break;			
      case '-':	
	       col = 13;
	       break;			
      case '+':	
	       col = 14;
	       break;			
      case ':':	
	       col = 15;
	       break;			
      case '>':	
	       col = 16;
	       break;			
      case '<':	
	       col = 17;
	       break;			
      case '=':	
	       col = 18;
	       break;			
      case '&':	
	       col = 22;
	       break;			
      case '\n':	
	       col = 23;	
	       break;			
      case ' ':	
	       col = 24;
	       break;			
      default:	
	       break;	
    }
  return col;

}

/*get token function*/

token get_token(FILE *filePtr, char nextChar, int line_number) {

  int state = 0;
  int nextState;
  int col;
  string token_instance = "";

  while (state >= 0 && state < 1000) {

    if (nextChar >= 'A' && nextChar <='Z')
      col = 21;
    if (nextChar >= 'a' && nextChar <= 'z')
      col = 19;
    if (nextChar >= '0' && nextChar <= '9')
      col = 20;

    col = operator_or_limiter(nextChar, col);

    nextState = FSA_TABLE[state][col];
   
    if (nextState < 0) {

      cout << "error\n";
      cout << "Character: " << nextChar << " Line Number:  " << line_number << "\n";
      fclose(filePtr);
      exit (1);
    }

    if (nextState > 999) {

      if (nextState == 1000) {

	fseek(filePtr, -1, SEEK_CUR);
	for (int x = 0; x < 12; x++) {

	  if(KEYWORDS[x] == token_instance)
	    nextState = nextState + ADD_TO_GET_KEYWORD + x;
	}
      }

      if (nextState == 1001)
	fseek(filePtr, -1, SEEK_CUR);

      if (nextState >= 1014 && nextState < 1032)
	token_instance += nextChar;

      nextState = nextState % 1000;

      token final_token = {INSTANCES[nextState], token_instance, line_number};
      return final_token;
    }

    else {

      state = nextState;
      token_instance += nextChar;
      nextChar = fgetc(filePtr);
    }
  }

}
