//token.h
//H file for token struct
//Justin Henn
//Assignment 1
//3/15/18

#ifndef TOKEN_H
#define TOKEN_H
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

using namespace std;


//string instances[] = {IDTk, digitTk, startTk, stopTk, iterTk, voidTk, returnTk, readTk, printTk, programTk, iffTk, thenTk, letTk, =Tk, <Tk, >Tk, :Tk, +Tk, -Tk, *Tk, /Tk, %Tk, .Tk, Tk, )Tk, ,Tk, {Tk, }Tk, ;Tk, [Tk, ]Tk,EOFTk,};

typedef struct {

  string token_type;
  string instance;
  int line_number;

} token;

#endif
