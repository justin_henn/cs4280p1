//testScanner
//This is the testScanner to run the scanner program
//Justin Henn
//Assignment 1
//3/15/18
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <string.h>
#include "scanner.h"
#include "token.h"

using namespace std;

/*Test scanner function that takes a char and sends to the scanner*/

void test_scanner(FILE* file) {

  char first_char;
  int line_number = 1;
  token output_token;

  while ((first_char = fgetc(file)) !=EOF) {

    if (first_char == ' ') {

      ;
    }

    else if (first_char == '\n') {

      line_number++;
    }

    else if (first_char == '&') {

      while((first_char = fgetc(file)) != '&')
	;
    }

    else {

      output_token = get_token(file, first_char, line_number);
      cout << output_token.token_type << " " << output_token.instance << " " << output_token.line_number << "\n";
    }
  }
  output_token = get_token(file, first_char, line_number);
  cout << output_token.token_type << "\n";
}


    
