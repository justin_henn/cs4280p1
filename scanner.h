//scanner.h
//H file for the scanner
//Justin Henn
//Assignment 1
//3/15/18

#ifndef SCANNER_H
#define SCANNER_H
#include "token.h"

token get_token(FILE *filePtr, char nextChar, int line_number);

#endif
