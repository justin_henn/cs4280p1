//main
//This is the main file for the scanner
//Justin Henn
//Assignment 1
//3/15/18
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <string.h>
#include "testScanner.h"

using namespace std;

int main(int argc, char **argv) {

/*Variables needed from main*/

  FILE*  myfile;
  char arg1[1024];
  const char ADD_ON[] = ".sp18";
  string filename = "";
  
/*Check if file argument*/

  if (argc > 1) {

    strcpy(arg1, argv[1]);
    strcat(arg1, ADD_ON);

    if ((myfile = fopen(arg1, "r")) == NULL) {

      printf("Cannot open file.\n");
      return -1;
    }


  }

  /*If no file argument*/

  else {

    ofstream ofile;
    ofile.open("input.txt");
    string s;

    if (!ofile.is_open()) {
 
      cout << "Unable to open file\n";
      return 0;
  }
    while(getline(cin, s)) {

      ofile << s << "\n";
    }
    ofile.close();

    if ((myfile = fopen("input.txt", "r")) == NULL) {

      printf("Cannot open file.\n");
      return -1;
    }
  }

  fseek(myfile, 0, SEEK_END);
  int size = ftell(myfile);

  if (size == 0) {
    printf("file is empty\n");		
  }
  
  rewind(myfile);
  
  
  /*get output file names*/

  if (argc > 1) {

    filename = argv[1];
  }
  else {

    filename = "out";
  }

  /*do scanner operations*/
 
  test_scanner(myfile);
  fclose(myfile);
 }
